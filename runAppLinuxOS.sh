#!/bin/sh

./gradlew build -x check
docker network ls|grep labdsoft_network > /dev/nul || docker network create --driver bridge labdsoft_network
docker-compose up -d --build 