@ECHO OFF
call gradlew build -x check
call docker network ls|grep labdsoft_network > nul || docker network create --driver bridge labdsoft_network
call docker-compose up -d --build
